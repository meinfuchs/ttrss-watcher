function saveOptions(e) {
    var baseurl = document.querySelector("#baseurl").value;
    // character
    if (baseurl.substr(-1) != '/') {         // If the last character is not a slash
        baseurl = baseurl + '/';  // Append a slash to it.
    }

    var settings = {
        settings: {
            baseurl: baseurl,
            login: document.querySelector("#login").value,
            singleuser: document.querySelector("#singleuser").checked,
            updateinterval: Number(document.querySelector("#updateinterval").value),
            backgroundcolor: document.querySelector("#backgroundcolor").value
        },
    };
    
    var result = browser.storage.local.set(settings);
    browser.runtime.sendMessage({
        type: "settings-updated",
        message: settings,
    });    
    e.preventDefault();
}

function restoreOptions() {
    var getSettings = browser.storage.local.get("settings");
    getSettings.then((res) => {
        const {settings} = res || {
            settings: {
                baseurl: 'http://example.com/tt-rss',
                login: 'admin',
                singleuser: false,
                updateinterval: 2,
                backgroundcolor: 'Firefox red'
            },
        }
        document.querySelector("#baseurl").value = settings.baseurl ;
        document.querySelector("#login").value = settings.login;
        document.querySelector("#singleuser").checked = settings.singleuser;
        document.querySelector("#updateinterval").value = settings.updateinterval;
        document.querySelector("#backgroundcolor").value = settings.backgroundcolor;
    });
}

function localize() {
    document.querySelector("#baseurl_label").innerHTML = browser.i18n.getMessage("BaseURL_title");
    document.querySelector("#login_label").innerHTML = browser.i18n.getMessage("Login_title");
    document.querySelector("#singleuser_label").innerHTML = browser.i18n.getMessage("SingleUser_title");
    document.querySelector("#updateinterval_label").innerHTML = browser.i18n.getMessage("UpdateInterval_title");
    document.querySelector("#backgroundcolor_label").innerHTML = browser.i18n.getMessage("BackgroundColor_title");
    document.querySelector("#savebutton").innerHTML = browser.i18n.getMessage("SaveButton_title");
}

localize();
restoreOptions();
document.querySelector("form").addEventListener("submit", saveOptions);
