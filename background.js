// handler for left mouse click: open tt-rss
function buttonClick(){
    check();
    var getSettings =  browser.storage.local.get("settings");
    getSettings.then((res) => {
        const {settings} = res;
        var baseurl = settings.baseurl;
        if(baseurl !== "http://example.com/tt-rss/") {
            var querying = browser.tabs.query({url: baseurl + "*"});
            querying.then((tab) => {
                if(tab.length > 0) {
                    browser.tabs.update(tab[0].id, { active: true })
                } else {
                    browser.tabs.create({url:baseurl, active:true});
                }});
        } else {
            // the url has not been configured yet so it is a good idea to do so
            browser.runtime.openOptionsPage();
        }
    });
};

function check() {
    var getSettings = browser.storage.local.get("settings");
    getSettings.then((res) => {
        const {settings} = res;
        if(settings.baseurl !== "http://example.com/tt-rss/") {
            var login = settings.login;
            if(settings.singleUser) {
                login = "admin"
            };
            
            var myHeaders = new Headers();
            myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
            var myFormData = new FormData();
            myFormData.append('op', 'getUnread');
            myFormData.append('fresh', 1);
            myFormData.append('login', login);
            var init = { 
                method: 'POST', 
                body: 'op=getUnread&fresh=1&login='+login,
                headers: myHeaders
            }
            var request = new Request(settings.baseurl+"public.php", init);
            fetch(request).then(analyze).catch(handleError);
        }
    })
};

function analyze(response) {
    // check for successful response
    if(response.status == "200") {
        response.text().then((body) => {
            var answer = body.split(";");
            var unread = parseInt(answer[0]);
            if (isNaN(unread)) unread = 0;

            // console.log("update, count="+unread);
            var tooltip = "";
            
            if(unread == 0) {
                var zero = browser.i18n.getMessage("zerocount_id")
                browser.browserAction.setTitle( {title:browser.i18n.getMessage("unreadcount_id", zero)});
                browser.browserAction.setIcon({path:"data/normal.png"});
                browser.browserAction.setBadgeText({text:""});
            } else if (unread >0) {
                browser.browserAction.setTitle( {title:browser.i18n.getMessage("unreadcount_id", unread.toString())});
                browser.browserAction.setIcon({path:"data/alert.png"});
                browser.browserAction.setBadgeText({text:unread.toString()});
            } else {
                browser.browserAction.setTitle( {title:browser.i18n.getMessage("error_id", answer[1].trim())});
                browser.browserAction.setIcon({path:"data/error.png"});
                browser.browserAction.setBadgeText({text:""});
            };
        });
    } else if (response.status == "404") {
        // console.log(response.text);
        browser.browserAction.setTitle( {title:browser.i18n.getMessage("urlerror_id")});
        browser.browserAction.setIcon({path:"data/error.png"});
        browser.browserAction.setBadgeText({text:""});
    } else {
        // console.log(response.text);
        browser.browserAction.setTitle( {title:browser.i18n.getMessage("updateerror_id", response.statusText)});
        browser.browserAction.setIcon({path:"data/error.png"});
        browser.browserAction.setBadgeText({text:""});
    }
};

function handleError(error) {
    // console.log(response.text);
    browser.browserAction.setTitle( {title:browser.i18n.getMessage("error_id", error.message)});
    browser.browserAction.setIcon({path:"data/error.png"});
    browser.browserAction.setBadgeText({text:""});
};


function handleInstalled(details) {
    if(details.reason=="install") {
        // first time installation so initialize the settings
        browser.storage.local.set({
            settings: {
                baseurl: 'http://example.com/tt-rss',
                login: 'admin',
                singleuser: false,
                updateinterval: 2,
                backgroundcolor: 'Firefox red'
            },
        });
        browser.notifications.create("install-notification", {
            type:"basic",
            message:browser.i18n.getMessage("InstallNotification_message"),
            iconUrl:"data/alert.png",
            title:"Tiny Tiny RSS Watcher"
        });
        browser.notifications.onClicked.addListener(function(notificationId) {
            browser.runtime.openOptionsPage();
        });
    } else if(details.reason == "update") {
	var manifest = browser.runtime.getManifest();
        browser.notifications.create("update-notification", {
            type:"basic",
            message:browser.i18n.getMessage("UpdateNotification_message", manifest.version),
            iconUrl:"data/alert.png",
            title:"Tiny Tiny RSS Watcher"
        });
        browser.notifications.onClicked.addListener(function(notificationId) {
            browser.tabs.create({url:"https://bitbucket.org/meinfuchs/ttrss-watcher/wiki/News%20for%20Version%203.1", active:true});
        });
    }
}

function onUpdateSettings(settings) {
    browser.alarms.clear("check");
    var interval = settings.updateinterval;
    browser.alarms.create("check", {delayInMinutes:interval, periodInMinutes:interval});
    browser.browserAction.setBadgeBackgroundColor({color:settings.backgroundcolor});
    browser.alarms.onAlarm.addListener(check);
    check();
}

// add the listeners
browser.browserAction.onClicked.addListener(buttonClick);
browser.runtime.onInstalled.addListener(handleInstalled);
browser.runtime.onMessage.addListener(msg => {
    if(msg.type == "settings-updated") {
        const {settings} = msg.message;
        onUpdateSettings(settings);
    }
});
var getSettings =  browser.storage.local.get("settings");
getSettings.then((res) => {
    const {settings} = res;
    onUpdateSettings(settings);
});
